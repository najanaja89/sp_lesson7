﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace sp_lesson7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task task = new Task((state)=>   //принимает только Action
            //{
            //   Thread.Sleep(3000);
            //}, new object());

            //1 способ
            //Task task = new Task(() =>
            //{
            //    Thread.Sleep(3000);
            //    Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} Задача завершена");
            //});
            //Некторое строк кода спустя
            //task.Start();

            //2 способ
            //Task.Factory.StartNew(() => //принимает только Action и Func
            //{
            //    Thread.Sleep(3000);
            //    Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} Задача завершена");
            //    // опционально возвращение результата
            //    return 0;           
            //});

            //3 способ
            //var task = Task.Run(() => //принимает Action и Func
            //{
            //    Thread.Sleep(3000);
            //    Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} Задача завершена");
            //    // опционально возвращение результата
            //    return 0;
            //});

            //Только для получения результата
            //Console.WriteLine(task.Result);

            //Для отмены задачи
            var cancelletionSource = new CancellationTokenSource();
            var cancelletionToken = cancelletionSource.Token;

            var cancellationTask = Task.Run(() => //принимает Action и Func
            {
                Thread.Sleep(200);
                Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} Задача завершена");
                // опционально возвращение результата
                return 0;
            }, cancelletionToken);

            Thread.Sleep(300);
            cancelletionSource.Cancel();

            Console.WriteLine("Главный поток завершил работу");
            Console.ReadLine();


        }

        private static Task DoSomeThingl()
        {
            //return Task.Run(()=>{}); -  плохая идея;
            return Task.CompletedTask; //Полезно для async-await
        }

        private static Task<bool> DoSomeThingBool()
        {
            try
            {
                //для отмены - Task.FromCancelled
                //Передача рзультата, можно и вне блока Try
                return Task.FromResult(true); 
            }
            catch (Exception exception)
            {
                return Task.FromException<bool>(exception);
            }
        }

    }
}
